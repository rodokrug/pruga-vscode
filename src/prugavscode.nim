when not defined(js):
  {.error: "This module only works on the JavaScript platform".}

import platform/vscodeApi
# import platform/js/[jsre, jsString, jsNodeFs, jsNodePath]

import std/jsconsole
# from std/strformat import fmt
# from std/os import `/`

import std/strformat
import std/strutils

# from spec import ExtensionState

const TERMINAL_NAME = "Ⱂⱃⱆⰳⰰ terminal"
const MAKE_SCRIPT = "./make.fish"
const PLAY_SCRIPT = "./make.fish"

vscode.window.setStatusBarMessage "Ⱂⱃⱆⰳⰰ disabled"

type

  Actions = enum
    playAction = (0, "play")
    makeAction = (1, "make")

  Commands = seq[cstring]

var terminal: VscodeTerminal


proc loadConfig(): Commands =

  let config = vscode.workspace.getConfiguration("pruga")

  for action in Actions:

    console.log &"Ⱂⱃⱆⰳⰰ config {action}"

    if config.has $action:
      result.add config.getStr $action
    else:
      case action
      of playAction:
        result.add PLAY_SCRIPT
      of makeAction:
        result.add MAKE_SCRIPT

  console.log "\t" & result.join "\n\t"

var commands: Commands = loadConfig()

proc runScript(action: Actions): void =

  let editor = vscode.window.activeTextEditor

  if editor.isNil():
    console.log "Ⱂⱃⱆⰳⰰ: Editor není aktivní"
    return

  let document = editor.document

  let fileName = document.fileName

  console.log "Ⱂⱃⱆⰳⰰ {action} {fileName}"

  discard document.save().then(proc(success: bool): void =
    if success:

      case action
      of playAction:
        terminal.sendText &"{commands[0]} {fileName}"
      of makeAction:
        terminal.sendText &"{commands[1]} {fileName}"
  )

  # terminal.show(true)
  # terminal.sendText(commandText)

proc playScript(): void =

  runScript playAction

proc makeScript(): void =

  runScript makeAction

proc activate*(context: VscodeExtensionContext): void =
  console.log "Ⱂⱃⱆⰳⰰ activate"

  terminal = vscode.window.createTerminal(TERMINAL_NAME)
  terminal.show()
  vscode.window.setStatusBarMessage "Ⱂⱃⱆⰳⰰ enabled"

  vscode.commands.registerCommand(&"pruga.{playAction}", playScript)
  vscode.commands.registerCommand(&"pruga.{makeAction}", makeScript)
  # context.subscriptions.push command


proc deactivate*(): void =
  terminal.close()
  vscode.window.setStatusBarMessage "Ⱂⱃⱆⰳⰰ disabled"
#   discard onClose()
#   discard closeAllNimSuggestProcesses()
#   fileWatcher.dispose()

var module {.importc.}: JsObject
module.exports.activate = activate
module.exports.deactivate = deactivate
