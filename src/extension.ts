'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import terminalRunner from './terminal'

vscode.window.setStatusBarMessage(`Ⱂⱃⱆⰳⰰ disabled`)

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    const path = require('path')

    const pruga_config = <any>vscode.workspace.getConfiguration('pruga')
    const script_devel = path.join(vscode.workspace.rootPath, pruga_config.script)
    const pruga_make = pruga_config.make

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log("Ⱂⱃⱆⰳⰰ - script: ", script_devel);
    console.log("Ⱂⱃⱆⰳⰰ - make: ", pruga_make);
    vscode.window.setStatusBarMessage(`Ⱂⱃⱆⰳⰰ enabled`);

    let terminal = terminalRunner.getTerminal()
    terminal.show()

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable_script = vscode.commands.registerCommand('pruga-script', () => {

        const editor = vscode.window.activeTextEditor;

        if (!editor) {
            console.log('Ⱂⱃⱆⰳⰰ: Editor není aktivní');
            return; // No open text editor
        }

        const document = editor.document

        document.save()
            .then(
                () => {

                    terminalRunner.executeInTerminal(`${script_devel} ${path.relative(vscode.workspace.rootPath, document.fileName)}`)

                }
            )
    });

    context.subscriptions.push(disposable_script);

    let disposable_make = vscode.commands.registerCommand('pruga-make', () => {

        const editor = vscode.window.activeTextEditor;

        if (!editor) {
            console.log('Ⱂⱃⱆⰳⰰ: Editor není aktivní');
            return; // No open text editor
        }

        const document = editor.document

        document.save()
            .then(
                () => {

                    terminalRunner.executeInTerminal(`${pruga_make} ${path.relative(vscode.workspace.rootPath, document.fileName)}`)

                }
            )
    });

    context.subscriptions.push(disposable_make);

}

// this method is called when your extension is deactivated
export function deactivate() {
    vscode.window.setStatusBarMessage(`Ⱂⱃⱆⰳⰰ disabled`)
}