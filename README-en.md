# Ⱂⱃⱆⰳⰰ (Pruga) vscode extension


This extension does a simple but very powerful thing. It turns the source code editor into a development environment of unlimited possibilities.

The whole thing is based on a completely primitive idea. You use a keyboard shortcut to run a script in the terminal. Before that, it saves the file you're editing.

I called the script `./make.fish` because I use the *fish shell*, but you might as well use `./make.sh` for the commonly used *bash shell* or something that works on your machine, whether your Linux, windows, or apple.

> Basically, it doesn't matter too much what shell you use. Even though the shell is also a programming language, I personally try to keep the code in this script very short, ideally one line that will run another tool, compiler, make, webpack, ... . However, the `make.fish` script is a good intermediate layer, I like to try different things and occasionally come across something that needs to be run repeatedly in the command line. And in this script, I write it down and run it and continually edit it.

```sh
#!/usr/bin/env fish

# Here you can write shell script code

echo "Пруга is the simplest and very powerfull 'build system'"

echo 'SHELL: Ⱂⱃⱆⰳⰰ run make.fish'

# comment unused commands

# Do you use make for build your program?
#make

# Do you not like shell? Are you like python? Your make.fish can run play.py.

# python /play.py $1
# or with shebank #!/usr/bin/env python
./play.py $1

# Do you use PHP?
php ./play.php $1

# Do you like excellent Nim language?
nim compile --app:console -d:release --out:bin/pruga-make ./cli/pruga/make.nim

```

If you use Python, your make.fish can run play.py.

```python
#!/usr/bin/env python

import sys

print('PYTHON: Пруга spouští play.py')
print(sys.argv)

```

PHP is better for you?


```php
<?php

echo "Пруга spouští play.php\n";
echo join(' ', $argv);
echo "\n";

```

Nim is better for you?


```nim

import std/terminal

styledEcho fgBlue, "I like", fgYellow, "Nim"

```

You can use any commands, languages, scripts ...


## project or file

Practice has shown that it is good to have two scripts mapped to two keyboard shortcuts. The scripts are similar

- make.fish
- play.fish

it's up to you what you write in them and how you use them. It's very configurable, but this makes sense.

- make.fish: is run with ctrl+shift+c and I have commands in the script to build the whole project. Often you are editing a file that is imported by the main file and need to compile the whole project with a quick keystroke.

- play.fish: is started with ctrl+shift+s and in the script I have a command to run or compile the currently saved file.

## pruga-make

`pruga-make` is program mainly for compiling `Nim` programs with several presets:

- `nim` files from './cli' directory compile to './bin' directory 
- `nim` files from './pkg' directory compile to './tests' directory and run this script as test
- other `nim` files compile usually
- `python` and `php` files run in terminal

## Default keybinding

for run `make.fish`:

`c` - as *compile*

* mac: cmd+shift+c
* window-linux: ctrl+shift+c

for run `play.fish`:

`s` - as *save*

* mac: cmd+shift+s
* window-linux: ctrl+shift+s
